FROM archlinux/base

RUN set -e; \
    set -x; \
    pacman -Syu --noconfirm --needed base-devel \
                                     arm-none-eabi-binutils \
                                     arm-none-eabi-gcc \
                                     arm-none-eabi-newlib \
                                     doxygen \
                                     graphviz \
                                     make \
                                     python \
                                     ruby; \
    pacman -Scc --noconfirm
